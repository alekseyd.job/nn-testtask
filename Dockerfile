FROM openjdk:8-jre-buster

WORKDIR /app

ENV SENCHA_VERSION=7.6.0.87
ENV OPENSSL_CONF=/etc/ssl
ENV _JAVA_OPTIONS="-Xms1536m -Xmx3072m"

RUN curl -o /cmd.run.zip http://cdn.sencha.com/cmd/$SENCHA_VERSION/no-jre/SenchaCmd-$SENCHA_VERSION-linux-amd64.sh.zip && \
  unzip -p /cmd.run.zip > /cmd-install.run && \
  chmod +x /cmd-install.run && \
  /cmd-install.run -q -Dall=true -dir /opt/Sencha/Cmd/$SENCHA_VERSION && \
  install -dm777 -o root -g root /opt/Sencha/Cmd/repo && \
  rm /cmd-install.run /cmd.run.zip && \
  ln -s /opt/Sencha/Cmd/$SENCHA_VERSION/sencha /opt/Sencha/sencha && \
  ln -s /opt/Sencha/Cmd/$SENCHA_VERSION/sencha /bin/sencha
