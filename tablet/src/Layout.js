Ext.define('TestTask.Layout', {
  extend: 'Ext.tab.Panel',
  xtype: 'app.layout',

  requires: [
    'TestTask.Task'
  ],

  viewModel: 'model',
  controller: 'default',

  defaults: {
    tab: {
      iconAlign: 'top'
    }
  },

  tabBarPosition: 'top',

  items: [
    {
      title: 'Информация',
      iconCls: 'x-fa fa-info',
      padding: 10,
      layout: 'fit',
      bind: {
        html: '{taskInfo}'
      }
    },{
      title: 'Задача',
      iconCls: 'x-fa fa-check',
      layout: 'vbox',
      items: [{
        xtype: 'toolbar',
        docked: 'bottom',
        items: [{
          xtype: 'numberfield',
          label: 'Количество витков',
          errorTarget: 'under',
          minValue: 1,
          required: true,
          requiredMessage: 'Поле не может быть пустым',
          maxValueText: 'Введенное значение больше допустимого',
          minValueText: 'Введенное значение меньше допустимого',
          bind: {
            value: '{loopValue}',
            maxValue: '{loopLimit}'
          },
        }, '->', {
          xtype: 'button',
          text: 'Обновить',
          iconCls: 'x-fa fa-sync',
          iconAlign: 'right',
          handler: 'onRefreshFibonacci'
        }]
      }, {
        xtype: 'tt',
        bind: {
          loops: '{loopValue}'
        }
      }]
    }
  ]
});