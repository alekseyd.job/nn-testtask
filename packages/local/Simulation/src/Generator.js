/**
 * Генератор данных для апи.
 */
Ext.define("Simulation.Generator", {
    mixins: ['Ext.mixin.Observable'],

    config: {
        isRegExpUrl: true,
        type: 'json',
        method: 'GET',
        url: '',
        delay: 1,
        skip: false
    },

    constructor(config) {
        this.initConfig(config);
    },

    /**
     * Подключаем возможность использования regExp
     * @returns {RegExp}
     */
    getUrl() {
        return this.getIsRegExpUrl() ? new RegExp(this._url) : this._url;
    },

    /**
     * Генерируем данные
     * @param request
     * @returns {Array}
     */
    generate(request) {
        return [];
    },

    /**
     * Генерируем случайную дату
     * @returns {*}
     * @private
     */
    randomDateStr() {
        let start = new Date(2012, 0, 1),
            end = new Date('2020-12-31'),
            res = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));

        return Ext.Date.format(res, 'c').replace('+03:00', '');
    },

    /**
     * Генерируем случайное число с плавоющей точкой
     * @param min
     * @param max
     * @param signs
     */
    randomNumber(min = 1000000, max = 9999999, signs = 2) {
        return (Math.random() * (max - min) + min).toFixed(signs);
    }
});
