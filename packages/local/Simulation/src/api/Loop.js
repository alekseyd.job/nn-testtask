/**
 * Генератор данных витков спирали Фибоначчи
 */
Ext.define('Simulation.api.Loop', {
    extend: 'Simulation.Generator',

    config: {
        url: '/api/loopCount',
        skip: false,
        delay: 300
    },

    /**
     * Генерируем значение для
     * @returns {number}
     */
    generate() {
        return Math.ceil(Math.random() * (10 - 1) + 1);
    }
});