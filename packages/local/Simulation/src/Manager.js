/**
 * Менеджер эмуляции программного интерфейса
 */
Ext.define("Simulation.Manager", {
    requires: [
        //<if simulate>
        'Simulation.api.*',
        //</if>
        'Ext.ux.ajax.SimManager'
    ],

    singleton: true,

    /**
     * Инициализируем симуляцию если необходимо
     */
    constructor() {
        this.callParent(arguments);

        if (cfg.simulate) {
            Ext.ux.ajax.SimManager.init({
                defaultSimlet: null
            }).register(
                this.getRegisterData()
            );
        }
    },
    //</if>

    /**
     * Генерируем данные для sim менеджера
     * @returns {Array}
     */
    getRegisterData(path=Simulation.api) {
        let registerData = [];
        Ext.Object.each(path, (key, obj) => {
            if (obj.$className) {
                let simObject = Ext.create(obj.$className),
                    config = Ext.clone(simObject.getConfig());
                if (config.skip) {
                    return true;
                }
                config.data = function (request) {
                    console.groupCollapsed('Intercepted request: ' + request.url);
                    console.info('Method: ' + request.method);
                    console.info('Handler: ' + simObject.$className);
                    console.info('Params:');
                    console.dir(request.params);
                    const response = {
                        "code": 200,
                        "status": true,
                        "data": simObject.generate.call(simObject, request)
                    };
                    console.info('Response:');
                    console.dir(response);
                    console.groupEnd();
                    return response;
                };
                registerData.push(config);
            } else {
                registerData = registerData.concat(this.getRegisterData(path[key]));
            }
        });
        return registerData;
    }
});
