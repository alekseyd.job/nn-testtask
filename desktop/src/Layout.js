Ext.define('TestTask.Layout', {
  extend: 'Ext.tab.Panel',
  xtype: 'app.layout',

  requires: [
    'TestTask.Task'
  ],

  ui: 'navigation',

  viewModel: 'model',
  controller: 'default',

  tabBarHeaderPosition: 1,
  titleRotation: 0,
  tabRotation: 0,

  header: {
    layout: {
      align: 'stretchmax'
    },
    title: {
      bind: {
        text: '{applicationName}'
      },
      flex: 0
    },
    iconCls: 'fa-th-list'
  },

  tabBar: {
    flex: 1,
    layout: {
      align: 'stretch',
      overflowHandler: 'none'
    }
  },

  responsiveConfig: {
    tall: {
      headerPosition: 'top'
    },
    wide: {
      headerPosition: 'left'
    }
  },

  defaults: {
    bodyPadding: 20,
    tabConfig: {
      responsiveConfig: {
        wide: {
          iconAlign: 'left',
          textAlign: 'left'
        },
        tall: {
          iconAlign: 'top',
          textAlign: 'center',
          width: 120
        }
      }
    }
  },

  items: [{
    title: 'Информация',
    iconCls: 'fa-info',
    bind: {
      html: '{taskInfo}'
    }
  }, {
    title: 'Задача',
    iconCls: 'fa-check',
    layout: 'vbox',
    items: [{
      xtype: 'toolbar',
      width: '100%',
      items: [{
        xtype: 'numberfield',
        fieldLabel: 'Количество витков',
        labelWidth: 120,
        allowBlank: false,
        minValue: 1,
        blankText: 'Поле не может быть пустым',
        maxText: 'Введенное значение больше допустимого',
        minText: 'Введенное значение меньше допустимого',
        msgTarget: 'errorFieldLabel',
        bind: {
          value: '{loopValue}',
          maxValue: '{loopLimit}'
        },
      }, {
        xtype: 'label',
        id: 'errorFieldLabel',
        cls: 'errorFieldLabel',
      }, '->', {
        xtype: 'button',
        text: 'Получить данные с сервера',
        handler: 'onRefreshFibonacci'
      }]
    }, {
      xtype: 'tt',
      bind: {
        loops: '{loopValue}',
      },
    }]
  }]
});