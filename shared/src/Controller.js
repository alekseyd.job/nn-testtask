Ext.define('TestTask.Controller', {
  extend: 'Ext.app.ViewController',

  alias: 'controller.default',

  /**
   * Перестраиваем спираль Фибоначчи с сервера
   */
  onRefreshFibonacci: function () {
    Ext.Ajax.request({
      url: '/api/loopCount'
    }).then((res) => {
      const obj = Ext.decode(res.responseText);
      this.getViewModel().set('loopValue', obj.data);
    });
  },
});