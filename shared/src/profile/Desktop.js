/**
 * Профиль для desktop приложения
 */
Ext.define('TestTask.profile.Desktop', {
  extend: 'Ext.app.Profile',

  config: {
    /**
     * @cfg {int}
     */
    limit: 30,
  },

  /**
   * Проверка активности профайла
   * @returns Boolean
   */
  isActive: function () {
    return Ext.platformTags.desktop;
  },

  /**
   * Код для запуска профайла
   */
  launch: function () {
    console.log('Запуск Desktop приложения');
  }
});
