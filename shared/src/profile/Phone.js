/**
 * Профиль для устройства Phone
 */
Ext.define('TestTask.profile.Phone', {
  extend: 'Ext.app.Profile',

  config: {
    /**
     * @cfg {int}
     */
    limit: 10,
  },

  /**
   * Проверка активности профайла
   * @returns Boolean
   */
  isActive: function () {
    return Ext.platformTags.phone;
  },

  /**
   * Код для запуска профайла
   */
  launch: function () {
    console.log('Запуск Phone приложения');
  }
});
