/**
 * Профиль для планшетного приложения
 */
Ext.define('TestTask.profile.Tablet', {
  extend: 'Ext.app.Profile',

  config: {
    /**
     * @cfg {int}
     */
    limit: 20,
  },

  /**
   * Проверка активности профайла
   * @returns Boolean
   */
  isActive: function () {
    return Ext.platformTags.tablet;
  },

  /**
   * Код для запуска профайла
   */
  launch: function () {
    console.log('Запуск Tablet приложения');
  }
});
