Ext.define('TestTask.Task', {
  extend: 'Ext.Component',
  xtype: 'tt',

  config: {
    /**
     * Конфигурация количества витков спирали
     * @cfg {number} loops
     */
    loops: null
  },

  layout: 'fit',
  width: '100%',
  height: '100%',

  html: '<canvas />',

  /**
   * После рендеринга получаем ссылку на canvas
   */
  afterRender: function () {
    this.canvas = this.el.down('canvas').dom;
    this.ctx = this.canvas.getContext("2d");
  },

  /**
   * Обновляем canvas числом заданным пользователем
   * @param {number} loops
   */
  updateLoops: function (loops) {
    this.canvas.width = this.el.getWidth();
    this.canvas.height = this.el.getHeight();

    this.drawFibonacciSpiral({x:0, y:0}, {x:loops, y: loops});
  },

  /** ------------------ https://jsfiddle.net/a13c9bbj/2 ------------------------- **/
  drawFibonacciSpiral: function (p1, p2) {
    const width = this.ctx.canvas.width;
    const height = this.ctx.canvas.height;

    const center = {
      x: width / 2,
      y: height / 2
    };

    this.ctx.clearRect(0, 0, width, height);

    // Draw coord axis -> center viewport at 0,0
    this.drawStroke([{x:0, y:-center.y}, {x:0, y:center.y}], center, "gray");
    this.drawStroke([{x:-center.x, y:0}, {x:center.x, y:0}], center,"gray");

    // Draw spiral -> center viewport at 0,0
    this.drawStroke(this.getSpiral(p1, p2, this.getDistance({x:0,y:0},center)), center);
  },

  getDistance: function (p1, p2) {
    return Math.sqrt(Math.pow(p1.x-p2.x, 2) + Math.pow(p1.y-p2.y, 2));
  },

  getAngle: function (p1, p2) {
    return Math.atan2(p2.y-p1.y, p2.x-p1.x);
  },

  drawStroke: function (points, offset, strokeColor) {
    offset = offset || {x:0,y:0}; // Offset to center on screen
    strokeColor = strokeColor || "black";

    this.ctx.strokeStyle = strokeColor;
    this.ctx.beginPath();
    var p = points[0];
    this.ctx.moveTo(offset.x + p.x, offset.y + p.y);
    for(var i = 1; i < points.length; i++){
      p = points[i];
      this.ctx.lineTo(offset.x + p.x, offset.y + p.y);
    }
    this.ctx.stroke();  // draw it all
  },

  FibonacciGenerator: function () {
    var thisFibonacci = this;

    // Start with 0 1 2... instead of the real sequence 0 1 1 2...
    thisFibonacci.array = [0, 1, 2];

    thisFibonacci.getDiscrete = function(n){

      // If the Fibonacci number is not in the array, calculate it
      while (n >= thisFibonacci.array.length){
        var length = thisFibonacci.array.length;
        var nextFibonacci = thisFibonacci.array[length - 1] + thisFibonacci.array[length - 2];
        thisFibonacci.array.push(nextFibonacci);
      }

      return thisFibonacci.array[n];
    };

    thisFibonacci.getNumber = function(n){
      var floor = Math.floor(n);
      var ceil = Math.ceil(n);

      if (Math.floor(n) == n){
        return thisFibonacci.getDiscrete(n);
      }

      var a = Math.pow(n - floor, 1.15);

      var fibFloor = thisFibonacci.getDiscrete(floor);
      var fibCeil = thisFibonacci.getDiscrete(ceil);

      return fibFloor + a * (fibCeil - fibFloor);
    };

    return thisFibonacci;
  },

  getSpiral: function (pA, pB, maxRadius) {
    // 1 step = 1/4 turn or 90º

    var precision = 50; // Lines to draw in each 1/4 turn
    var stepB = 4; // Steps to get to point B

    var angleToPointB = this.getAngle(pA,pB); // Angle between pA and pB
    var distToPointB = this.getDistance(pA,pB); // Distance between pA and pB

    var fibonacci = new this.FibonacciGenerator();

    // Find scale so that the last point of the curve is at distance to pB
    var radiusB = fibonacci.getNumber(stepB);
    var scale = distToPointB / radiusB;

    // Find angle offset so that last point of the curve is at angle to pB
    var angleOffset = angleToPointB - stepB * Math.PI / 2;

    var path = [];

    var i, step , radius, angle, p;

    // Start at the center
    i = step = radius = angle = 0;

    // Continue drawing until reaching maximum radius
    while (radius * scale <= maxRadius){
      p = {
        x: scale * radius * Math.cos(angle + angleOffset) + pA.x,
        y: scale * radius * Math.sin(angle + angleOffset) + pA.y
      };

      path.push(p);

      i++; // Next point
      step = i / precision; // 1/4 turns at point
      radius = fibonacci.getNumber(step); // Radius of Fibonacci spiral
      angle = step * Math.PI / 2; // Radians at point
    }

    return path;
  }
});