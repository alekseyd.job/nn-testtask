/**
 * Класс конфигурации приложения
 */
Ext.define('TestTask.core.Config', {
  alternateClassName: 'cfg',
  singleton: true,

  simulate: true
});