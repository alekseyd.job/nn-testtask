Ext.application({
    extend: 'Ext.app.Application',
    name: 'TestTask',
    profiles: ['Desktop', 'Phone', 'Tablet'],
    requires: [
        'TestTask.*',
        'Simulation.Manager'
    ],
    mainView: 'TestTask.Layout',

    /**
     * Если приложение обновилось, перегружаем страницу
     */
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
          function (choice) {
              if (choice === 'yes') {
                  window.location.reload();
              }
          }
        );
    }
});
